import React from 'react'
import '../styles/styles.css'

export default class Order extends React.Component {

    constructor() {
        super()

        this.state = {
            id: '', name: '', email: '', phone: '', address: '', orderDate: '', total: ''

        }
    }
    handleSave(){
        if(this.state._id === undefined || this.state._id ==='')
            this.props.dispatch(addOrder(this.state))
        else    
            this.props.dispatch(updateOrder(this.state))
    }

    handleDelete(_id) {
        if (confirm('Do you want to delete?')) {
            this.props.dispatch({
                type: 'DELETE_ORDER',
                payload: _id
            })
            this.props.dispatch(deleteOrder(_id))

        }
    }
    handleView(_id){
        this.props.dispatch({type:'VIEW_ORDER',payload: _id})
        //View the only product without interfere with the fetched data
    }
    handleEdit(id){
        this.props.dispatch(getOrder(id))
    }
    
    handleChange(e) {
        let change = {}
        change[e.target.name] = e.target.value
        this.setState(change)
    }
    render() {
        return (
            <div className="panel">
                <div className='panelHeading'>
                    <h3>Order Form</h3>
                </div>
                <div className='panelBody'>
                    <div className='row'>
                        <div className='col2'>
                            <label>ID</label>
                            <input type="text" name='id' className='formControl' value={this.state.id || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                        <div className='col2'>
                            <label>Name</label>
                            <input type="text" name='name' className='formControl' value={this.state.name || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                        <div className='col2'>
                            <label>Email</label>
                            <input type="text" name='email' className='formControl' value={this.state.email || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                        <div className='col2'>
                            <label>Phone</label>
                            <input type="text" name='phone' className='formControl' value={this.state.phone || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col2'>
                            <label>Address</label>
                            <input type="text" name='address' className='formControl' value={this.state.address || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                        <div className='col2'>
                            <label>OrderDate</label>
                            <input type="text" name='orderDate' className='formControl' value={this.state.orderDate || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                        <div className='col2'>
                            <label>Total</label>
                            <input type="text" name='total' className='formControl' value={this.state.total || ''} onChange={this.handleChange.bind(this)} />
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col2'>

                            <input type='button' className='btn' value='Save' onClick={this.handleSave.bind(this)} />

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
