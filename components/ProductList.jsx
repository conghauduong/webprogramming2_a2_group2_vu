import React from 'react'
import '../styles/styles.css'

export default class ProductList extends React.Component{
    render(){
        return(

            <div className='panel'>
                  <div className='panelHeading'>
                   <h3>Product List</h3>
                  </div>
                  <div className='panelBody'>
                  <table>
                    <thead>
                        <tr>
                        <td>_id</td>
                        <td>ID</td>
                        <td>Product</td>
                        <td>Price</td>
                        <td>Description</td>
                        <td>Brand</td>
                        <td>Producer</td>
                        <td>Type</td>
                        <td>Del</td>
                        <td>Edit</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.products.map((product, i)=>
                            <tr key={i}>
                                <td>{product._id}</td>
                                <td>{product.productId}</td>
                                <td>{product.productName}</td>
                                <td>{product.price}</td>
                                <td>{product.description}</td>
                                <td>{product.brand}</td>
                                <td>{product.producer}</td>
                                <td>{product.productType}</td>

                                <td><a onClick={(e)=>{
                                    let id = product._id

                                    if(confirm('Do you want to delete?'))
                                        this.props.deleteProductClick(id)
                                    }
                                }>
                                <img className='icon' src='./icons/rubbish-bin.png'/></a>
                                </td>

                                <td><a onClick={(e)=>{
                                    let id = product._id
                                    this.props.editProductClick(id)
                                    }
                                }>
                                <img className='icon' src='./icons/new-file.png'/></a>
                                </td>
                                
                                

                            </tr>
                        )}
                    </tbody>
                  </table>
                  </div>

                </div>


        )
    }
}