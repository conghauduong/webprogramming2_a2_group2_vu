import React from 'react'
import '../styles/styles.css'

export default class ProductPage extends React.Component{

    render(){
        return(

            <div className='panel'>
                  <div className='panelHeading'>
                   <h3>List of Product</h3>
                  </div>
                  <div className='panelBody'>
                  <table>
                    <thead>
                        <tr>
                        <td>Name</td>
                        <td>Price</td>
                        <td>Image</td>
                        <td>Show details</td>
                        <td>Add to cart</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.products.map((product, i)=>
                            <tr key={i}>
                                <td>{product.productName}</td>
                                <td>{product.price}</td>
                                <td><img id="img1" src = {product.imageUrl} width ='150px' height ='150px' /></td>
                                
                                <td><a onClick={(e)=>{
                                    let id = product._id
                                    var name = product.productName
                                    var price = product.price
                                    var description = product.description
                                    var brand = product.brand
                                    var producer = product.producer
                                    var type =product.productType
                                    
                                    if(!confirm("Get product details?"))
                                    this.props.getDetails(id)
                                    return(alert("Product Name : "+ name 
                                    +"\n"+ "Price : " + price
                                    +"\n"+ "Description : " + description
                                    +"\n"+ "Brand : " + brand
                                    +"\n"+ "Producer : " + producer
                                    +"\n"+ "Product type : " + type))

                                    }
                                }>
                                <img className='button' src='./icons/details.png' height = '35px' width = '150px' /></a>
                                </td>
                                <td><a onClick={(e)=>{
                                    let id = product._id
                                    let newState = {}
                                    if(confirm('Do you want to add this item to cart'))
                                    
                                    
                                    newState['id']= product.id
                                    newState['name']= product.name
                                    newState['price']= product.price
                                    newState['description']= product.description
                                    newState['brand']= product.brand
                                    newState['producer']= product.producer
                                    newState['imageUrl']= product.imageUrl
                                    product.exist = product.exist - 1
                                        
                                    this.props.addToCartClick(newState)

                                    }
                                }>
                                <img className='button' src='./icons/cart.png' width ='35px' height = '35px' /></a>
                                </td>

                            </tr>
                        )}
                    </tbody>
                  </table>
                  </div>

                </div>


        )
    }
}