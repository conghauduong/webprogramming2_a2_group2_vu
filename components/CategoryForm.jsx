import React from 'react'
import '../styles/styles.css'   

import Select from 'react-select'
import 'react-select/dist/react-select.css'

var myClassName = {
    width: '145px'
  }

var errorMessage = {}
export default class CategoryForm extends React.Component{


    constructor(props){
        super(props)

        const initialEditedCategory = {categoryId:'', categoryName: '',
            errorMessage:{
                categoryId: '',
                categoryName: ''
            }}
        this.state = initialEditedCategory
    }   

    componentWillReceiveProps(props){

        console.log(props)
        this.setState(props.editedCategory)
    }


    handleChange(e){

        //this to update only one property of a big object
        var change = {}
        change[e.target.name] = e.target.value
        this.setState(change)

        var errorMessage = {}
        
        if(e.target.name==='categoryName'){
            if(e.target.value.trim()===''){
                errorMessage[e.target.name] = 'Category must be not empty'
            }
            else{
                errorMessage[e.target.name] = ''
            }
        }
        if(e.target.name==='categoryId,., . . . '){
            if(e.target.value.trim()===''){
                errorMessage[e.target.name] = 'Id must be not empty'
            }
            else{
                errorMessage[e.target.name] = ''
            }
        }
       

        //console.log(errorMessage)
        this.setState({errorMessage: errorMessage})

    }

    // handleSubmit(e){  
        
                // if(this.state.errorMessage.categoryId=== '' || this.state.errorMessage.categoryName===''){
                //     alert('Please check the form')
                //     return false
                // }
        
            //     if(!this.state.isEditing)
            //             this.props.saveCategoryClick(this.state)
            //         else{
            //             this.props.updateCategoryClick(this.state)
            //         }         
            //         this.setState({categoryId: '', categoryName: '',price:'',description: '',
            //         brand: '',producer: '', isEditing: false})
                
            // }

    handleSubmit(e){
        if(this.state.categoryId == '' || this.state.categoryName ==''){
                    alert('Please check the form')
                    return false
                }   
        if(this.state._id===undefined || this.state._id==="")
            this.props.saveCategoryClick(this.state)
        else{
            this.props.updateCategoryClick(this.state)
        }         
    }

    render(){
        return(
            <div>

            <div className='panel'>
                <div className='panelHeading'>
                  <h3>Category Form</h3>
                </div>
                <div className='panelBody'>

                    <div className='row'>
                        <div className='col5'>
                        <label>ID</label>
                        <input type='text' name='categoryId' className='formControl' value={this.state.categoryId||''} onChange={this.handleChange.bind(this)}/> 
                        <div className='errorMessage'>{this.state.errorMessage.categoryId}</div>
                        </div>
                        
                        <div className='col5'>
                        <label>Category</label>
                        <input type='text' name='categoryName' className='formControl' value={this.state.categoryName||''} onChange={this.handleChange.bind(this)} /> 
                        <div className='errorMessage'>{this.state.errorMessage.categoryName}</div>
                        </div>

                        

                        

                    </div>

                    <div className='row'>
                        <div className='col5'>

                        <input type='button' className='btn' value='Save' onClick={this.handleSubmit.bind(this)}/>

                        <input type='button' className='btn' value='Add new' onClick={(e)=>{
                            this.props.addNewClick()
                        }}/>

                        </div>
                        
                    </div>
                 
                    <div className='row'></div>
                    
                  
                </div>
              </div>

            </div>
        )
    }
}