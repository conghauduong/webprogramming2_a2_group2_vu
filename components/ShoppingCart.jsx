import React from 'react'
import '../styles/styles.css'

export default class ShoppingCart extends React.Component{
    constructor() {
        super()

        this.state ={
            imageUrl: '',
            name: '', 
            price: '', 

        }
    
    }
    render(){
        return(

            <div className='panel'>
                  <div className='panelHeading'>
                   <h3>Shopping Cart</h3>
                   
                  </div>
                  <div className='panelBody'>
                  <table>
                    <thead>
                        <tr>
                        <td>Image</td>
                        <td>Product</td>
                        <td>Price</td>  
                        <td>Del</td>
                        <td><a href='/orderview' type='button' className='btn' >Checkout</a></td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.carts.map((cart, i)=>
                            <tr key={i}>
                                <td><img id="img1" src = {cart.imageUrl} width ='150px' height ='150px' /></td>
                                <td>{cart.name}</td>
                                <td>{cart.price}</td>
                                <td><a onClick={(e)=>{
                                    let id = cart._id

                                    if(confirm('Do you want to delete?'))
                                        this.props.deleteCartClick(cart._id)
                                    }
                                }>
                                <img className='icon' src='./icons/rubbish-bin.png'/></a>
                                </td>
                                
                                <div className='row'>
</div>

                            </tr>
                            
                        )}
                    </tbody>
                    <div className='row'>
                    </div>
                        
                  </table>
                  </div>

                </div>


        )
    }
}