import React from 'react'
import '../styles/styles.css'   

import Select from 'react-select'
import 'react-select/dist/react-select.css'

var myClassName = {
    width: '145px'
  }

var errorMessage = {}
export default class ProductForm extends React.Component{


    constructor(props){
        super(props)

        const initialEditedProduct = {productId:'', productName: '', price: '', 
        description: '', brand: '', producer: '', imageUrl: '', productType:'',
            errorMessage:{
                productId: '',
                productName: '',
                price: '',
                productType: ''
            }}
        this.state = initialEditedProduct
    }   

    componentWillReceiveProps(props){

        console.log(props)
        this.setState(props.editedProduct)
    }


    handleChange(e){

        //this to update only one property of a big object
        var change = {}
        change[e.target.name] = e.target.value
        this.setState(change)

        var errorMessage = {}
        
        if(e.target.name==='productName'){
            if(e.target.value.trim()===''){
                errorMessage[e.target.name] = 'Product must be not empty'
            }
            else{
                errorMessage[e.target.name] = ''
            }
        }
        if(e.target.name==='productId'){
            if(e.target.value.trim()===''){
                errorMessage[e.target.name] = 'Id must be not empty'
            }
            else{
                errorMessage[e.target.name] = ''
            }
        }
        if(e.target.name==='price'){
            if(e.target.value.trim()===''){
                errorMessage[e.target.name] = 'Price must be not empty'
            }
            else{
                errorMessage[e.target.name] = ''
            }
        }
        if(e.target.name==='productType'){
            if(e.target.value.trim()===''){
                errorMessage[e.target.name] = 'Type must be not empty'
            }
            else{
                errorMessage[e.target.name] = ''
            }
        }

        //console.log(errorMessage)
        this.setState({errorMessage: errorMessage})

    }

    // handleSubmit(e){  
        
                // if(this.state.errorMessage.productId=== '' || this.state.errorMessage.productName===''){
                //     alert('Please check the form')
                //     return false
                // }
        
            //     if(!this.state.isEditing)
            //             this.props.saveProductClick(this.state)
            //         else{
            //             this.props.updateProductClick(this.state)
            //         }         
            //         this.setState({productId: '', productName: '',price:'',description: '',
            //         brand: '',producer: '', isEditing: false})
                
            // }

    handleSubmit(e){
        if(this.state.productId == '' || this.state.productName ==''
    ||this.state.price == '' ||this.state.productType == '' ){
                    alert('Please check the form')
                    return false
                }   
        if(this.state._id===undefined || this.state._id==="")
            this.props.saveProductClick(this.state)
        else{
            this.props.updateProductClick(this.state)
        }         
    }

    render(){
        return(
            <div>

            <div className='panel'>
                <div className='panelHeading'>
                  <h3>Product Form</h3>
                </div>
                <div className='panelBody'>

                    <div className='row'>
                        <div className='col2'>
                        <label>ID</label>
                        <input type='text' name='productId' className='formControl' value={this.state.productId||''} onChange={this.handleChange.bind(this)}/> 
                        <div className='errorMessage'>{this.state.errorMessage.productId}</div>
                        </div>
                        
                        <div className='col2'>
                        <label>Product</label>
                        <input type='text' name='productName' className='formControl' value={this.state.productName||''} onChange={this.handleChange.bind(this)} /> 
                        <div className='errorMessage'>{this.state.errorMessage.productName}</div>
                        </div>

                        <div className='col2'>
                        <label>Price</label><input type='text' name='price' className='formControl' value={this.state.price||''} onChange={this.handleChange.bind(this)} /> 
                        <div className='errorMessage'>{this.state.errorMessage.price}</div>
                        </div>

                        <div className='col2'>
                        <label>Description</label>
                        <input type='text' name='description' className='formControl' value={this.state.description||''} onChange={this.handleChange.bind(this)}/> 
                        </div>

                    </div>

                    <div className='row'>
                        
                    <div className='col2'>
                        <label>Brand</label>
                        <input type='text' name='brand' className='formControl' value={this.state.brand||''} onChange={this.handleChange.bind(this)}/> 
                        </div>

                        <div className='col2'>
                        <label>Producer</label>
                        <input type='text' name='producer' className='formControl' value={this.state.producer||''} onChange={this.handleChange.bind(this)}/> 
                        </div>

                        <div className='col2'>
                        <label>Image Url</label>
                        <input type='text' name='imageUrl' className='formControl' value={this.state.imageUrl||''} onChange={this.handleChange.bind(this)} /> 
                        </div>

                        <div className='col2'>
                        <label>Product Type</label
                        ><input type='text' name='productType' className='formControl' value={this.state.productType||''} onChange={this.handleChange.bind(this)} /> 
                        <div className='errorMessage'>{this.state.errorMessage.productType}</div>
                        </div>

                    </div>


                    <div className='row'>
                        <div className='col2'>

                        <input type='button' className='btn' value='Save' onClick={this.handleSubmit.bind(this)}/>

                        <input type='button' className='btn' value='Add new' onClick={(e)=>{
                            this.props.addNewClick()
                        }}/>

                        </div>
                        
                    </div>
                 
                    <div className='row'></div>
                    
                  
                </div>
              </div>

            </div>
        )
    }
}