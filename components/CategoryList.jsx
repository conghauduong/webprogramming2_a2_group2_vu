import React from 'react'
import '../styles/styles.css'

export default class CategoryList extends React.Component{
    render(){
        return(

            <div className='panel'>
                  <div className='panelHeading'>
                   <h3>Category List</h3>
                  </div>
                  <div className='panelBody'>
                  <table>
                    <thead>
                        <tr>
                        <td>_id</td>
                        <td>ID</td>
                        <td>Category</td>
                        <td>Del</td>
                        <td>Edit</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.categorys.map((category, i)=>
                            <tr key={i}>
                                <td>{category._id}</td>
                                <td>{category.categoryId}</td>
                                <td>{category.categoryName}</td>
                                <td><a onClick={(e)=>{
                                    let id = category._id

                                    if(confirm('Do you want to delete?'))
                                        this.props.deleteCategoryClick(id)
                                    }
                                }>
                                <img className='icon' src='./icons/rubbish-bin.png'/></a>
                                </td>

                                <td><a onClick={(e)=>{
                                    let id = category._id
                                    this.props.editCategoryClick(id)
                                    }
                                }>
                                <img className='icon' src='./icons/new-file.png'/></a>
                                </td>

                            </tr>
                        )}
                    </tbody>
                  </table>
                  </div>

                </div>


        )
    }
}