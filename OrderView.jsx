import React, { Component } from 'react'

import { fetchOrder, addOrder, getOrder, deleteOrder, updateOrder } from './actions/order-actions.js'


import OrderForm from './components/OrderForm.jsx'
import OrderList from './components/OrderList.jsx'

import './styles/styles.css'


export default class OrderView extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        // When container was mounted, we need to start fetching todos.
        this.props.dispatch(fetchOrder())

    }

    render() {
        // let currentPath = window.location.pathname
        const { dispatch, editedOrder, orders } = this.props //,addedproduct

        return (
            <div className='container'>

                {/* <div>
                <div>
                  <a href="/">Home </a> ||
                  <a href="/about">About</a> ||
                  <a href="https://www.w3schools.com">Visit W3Schools</a>
                </div>   
                
                {currentPath.includes("/")? 
                    <About  />:
                    <App 
                    />
                }
              
            </div>       */}

                <div className='header'>
                    <h1>Order </h1>
                </div>


                <div className='row'>
                    <div className='col10'>
                        <OrderForm
                            saveOrderClick={(product) => dispatch(addOrder(product))}
                            updateOrderClick={(product) => dispatch(updateOrder(product))}
                            editedOrder={editedOrder}
                        />

                    </div>

                    <div className='row'></div>
                    <div className='col10'>
                        <OrderList orders={orders}
                            deleteOrderClick={(productId) => dispatch(deleteOrder(orderId))}
                            editOrderClick={(productId) => dispatch(editOrder(orderId))}
                        />

                    </div>
                    <div className='row'></div>
                </div>







            </div>








        )
    }
}

// function mapStateToProps(centralState) {
//    return {
//       products: centralState.productReducer,
//       editedProduct: centralState.editedProductReducer,
//       categorys: centralState.categoryReducer,
//       editedCategory: centralState.editedCategoryReducer
//       // addedproduct: centralState.productToCart
//    }
// }

// This function is used to provide callbacks to container component.
// function mapDispatchToProps(dispatch) {
//   return {
//     // This function will be available in component as `this.props.fetchTodos`
//     fetchPersons: function() {
//       dispatch(fetchPersons());
//     } 
//   };
// }
// export default connect(mapStateToProps)(Root)
// export default connect(mapStateToProps )(App)

