import React, { Component } from 'react'

import {fetchProducts, saveProduct, deleteProduct, editProduct, updateProduct} from './actions/product-actions.js' //addproductToCart
import {fetchCategorys, saveCategory, deleteCategory, editCategory, updateCategory} from './actions/category-actions.js'

import ProductForm from './components/ProductForm.jsx'
import ProductList from './components/ProductList.jsx'
import CategoryForm from './components/CategoryForm.jsx'
import CategoryList from './components/CategoryList.jsx'

import './styles/styles.css'


export default class App extends React.Component {

  constructor(props){
    super(props)
  }

  componentDidMount() {
    // When container was mounted, we need to start fetching todos.
    this.props.dispatch(fetchProducts())
    this.props.dispatch(fetchCategorys())
  
  }

  render() {
    // let currentPath = window.location.pathname
      const { dispatch, editedProduct, products, editedCategory, categorys } = this.props //,addedproduct

            return (
       <div className='container'>
 
        {/* <div>
                <div>
                  <a href="/">Home </a> ||
                  <a href="/about">About</a> ||
                  <a href="https://www.w3schools.com">Visit W3Schools</a>
                </div>   
                
                {currentPath.includes("/")? 
                    <About  />:
                    <App 
                    />
                }
              
            </div>       */}
            
          <div className='header'>
            <h1> 🇻🇳 Admin Page - Products & categories Manager 🇻🇳 </h1>
          </div>
          
         
          <div className='row'>
            <div className='col10'>
            <ProductForm 
                  saveProductClick = {(product)=>dispatch(saveProduct(product))} 
                  updateProductClick = {(product)=>dispatch(updateProduct(product))} 
                  editedProduct={editedProduct}
                  addNewClick = {()=>dispatch({type:'ADDNEW_PRODUCT'})}    
                />
                  
            </div>
            
            <div className='row'></div>
            <div className='col10'>
            <ProductList products={products} 
                  deleteProductClick = {(productId)=>dispatch(deleteProduct(productId))}
                  editProductClick = {(productId)=>dispatch(editProduct(productId))}
                  />
      
            </div>
            <div className='row'></div>
            </div>
            
         
          <div className='row'>
          
            <div className='col25'>
            <CategoryForm 
                  saveCategoryClick = {(category)=>dispatch(saveCategory(category))} 
                  updateCategoryClick = {(category)=>dispatch(updateCategory(category))} 
                  editedCategory={editedCategory}
                  addNewClick = {()=>dispatch({type:'ADDNEW_CATEGORY'})}    
                />
                
            </div>
            <div className='col75'>
            <CategoryList categorys={categorys} 
                  deleteCategoryClick = {(categoryId)=>dispatch(deleteCategory(categoryId))}
                  editCategoryClick = {(categoryId)=>dispatch(editCategory(categoryId))}
                  />
            </div>
            
            <div className='row'></div>
            
            
          </div>  
           
          </div>
          

          


      
    )
   }
}

// function mapStateToProps(centralState) {
//    return {
//       products: centralState.productReducer,
//       editedProduct: centralState.editedProductReducer,
//       categorys: centralState.categoryReducer,
//       editedCategory: centralState.editedCategoryReducer
//       // addedproduct: centralState.productToCart
//    }
// }

// This function is used to provide callbacks to container component.
// function mapDispatchToProps(dispatch) {
//   return {
//     // This function will be available in component as `this.props.fetchTodos`
//     fetchPersons: function() {
//       dispatch(fetchPersons());
//     } 
//   };
// }
// export default connect(mapStateToProps)(Root)
// export default connect(mapStateToProps )(App)

