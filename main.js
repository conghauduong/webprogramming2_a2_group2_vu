// import React from 'react'
// import ReactDOM from 'react-dom'
// import { render } from 'react-dom'
// import { createStore, applyMiddleware} from 'redux'
// import { Provider } from 'react-redux'

import App from './App.jsx'
import centralState from './reducers/reducers.js'
import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import Root from './Root.jsx'
import thunk from 'redux-thunk'
import ProductsView from './ProductsView.jsx'
import CartView from './CartView.jsx'
import OrderView from './Orderview.jsx'


// var centralState = combineReducers({
//     products, editedProduct,
//     categorys, editedCategory
// })


ReactDOM.render(

   <Provider store = {createStore(centralState, applyMiddleware(thunk))}>
      <Root />
   </Provider>, document.getElementById('app')
	
   
)
