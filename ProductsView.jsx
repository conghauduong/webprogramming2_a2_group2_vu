import React from 'react'
import {fetchPage,updateCart,getProduct} from './actions/productPage-action.js'
import ProductPage from './components/ProductPage.jsx'
import './styles/styles.css'
import { fetchProducts } from './actions/product-actions';
import { addToCart} from './actions/cart-actions.js'
export default class ProductsView extends React.Component {

    constructor(props){
      super(props)
    }
    componentDidMount() {
        // When container was mounted, we need to start fetching todos.
        this.props.dispatch(fetchProducts())
      }
      render() {
        // let currentPath = window.location.pathname
          const { dispatch, products} = this.props //,addedproduct
    
          return (
            <div className='container'>
                 
               <div className='header'>
                 <h1>🏵 💉 Flakka Shop 💉 🏵 </h1>
               </div>
              
               <div className='row'>
                 <div className='col10'>
                 <ProductPage products={products} 
                       addToCartClick= {(productId)=>dispatch(addToCart(productId))}
                       getDetails={(productId)=>dispatch(editProduct(productId))}
                 />
           
                 </div>
                 <div className='row'></div>
                 </div>
                 <div className='header'>
               </div>
               </div>         
         )
        }
     }
     
     // function mapStateToProps(centralState) {
     //    return {
     //       products: centralState.productReducer,
     //       editedProduct: centralState.editedProductReducer,
     //       categorys: centralState.categoryReducer,
     //       editedCategory: centralState.editedCategoryReducer
     //       // addedproduct: centralState.productToCart
     //    }
     // }
     
     // This function is used to provide callbacks to container component.
     // function mapDispatchToProps(dispatch) {
     //   return {
     //     // This function will be available in component as `this.props.fetchTodos`
     //     fetchPersons: function() {
     //       dispatch(fetchPersons());
     //     }
     //   };
     // }
     // export default connect(mapStateToProps)(Root)
     // export default connect(mapStateToProps )(App)
     
     