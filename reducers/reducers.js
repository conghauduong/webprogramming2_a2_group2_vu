import { combineReducers } from 'redux'



function productReducer(state = [], action){
  switch(action.type){
    case 'FETCH_PRODUCT_SUCCESS':
      state = action.products
      return state
    case 'DELETE_PRODUCT':
      var newProducts = state.filter(product=>{
        return product._id!=action.productId
      })
      return newProducts

    case 'ADD_PRODUCT_SUCCESS':
      var newProducts = [...state, action.product] 
      return newProducts;
    default:
      return state
  }
}


function editedProductReducer(state = {}, action){
  switch(action.type){
    case 'EDIT_PRODUCT_SUCCESS':
      return action.editedProduct
    case 'ADDNEW_PRODUCT':
      return {productId: '', productName: '', price:'', description: '',
      brand: '', producer:'',imageUrl: '', productType:''}
    default: 
      return state
  }
}

function categoryReducer(state = [], action){
  switch(action.type){
    case 'FETCH_CATEGORY_SUCCESS':
      state = action.categorys
      return state
    case 'DELETE_CATEGORY':
      var newCategorys = state.filter(category=>{
        return category._id!=action.categoryId
      })
      return newCategorys
    case 'ADD_CATEGORY_SUCCESS':
      var newCategorys = [...state, action.category] 
      return newCategorys;
    default:
      return state
  }
}
function editedCategoryReducer(state = {}, action){
  switch(action.type){
    case 'EDIT_CATEGORY_SUCCESS':
      return action.editedCategory
    case 'ADDNEW_CATEGORY':
      return {categoryId: '', categoryName: ''}
    default: 
      return state
  }
}

function cartReducer(state = [], action){
  switch(action.type){
    case 'FETCH_CART_SUCCESS':
      state = action.carts
      return state
    case 'DELETE_CART':
      var newCarts = state.filter(cart=>{
        return cart._id!=action.cartId
      })
      return newCarts
    case 'ADD_CART_SUCCESS':
      var newCarts = [...state, action.cart] 
      return newCarts;
    default:
      return state
  }
}

function orderReducer(state = [], action){
  switch(action.type){
    case 'FETCH_ORDER_SUCCESS':
      state = action.orders
      return state
    case 'DELETE_ORDER':
      var newOrders = state.filter(order=>{
        return order._id!=action.orderId
      })
      return newOrders
    case 'ADD_ORDER_SUCCESS':
      var newOrders = [...state, action.order] 
      return newOrders;
    default:
      return state
  }
}

const centralState = combineReducers({
   productReducer, editedProductReducer,
   categoryReducer, editedCategoryReducer,
   cartReducer, orderReducer
  //  productToCart
  
})

export default centralState