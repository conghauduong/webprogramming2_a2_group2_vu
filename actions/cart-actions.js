export function fetchCart(){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/shoppingcarts")
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({
                type: 'FETCH_CART_SUCCESS',
                carts: data
            })
        })   
    }
}

export function addToCart(product){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/shoppingCarts", {
            method: 'POST',
            headers: {
                'Content-type' : 'application/json',
                'Accept' : 'application/json'

            },
            body: JSON.stringify(product)
        })
        .then (function(res){
            return res.json()
        })
        .then(data=>dispatch(fetchCart())
    )
    }
}

export function deleteCart(id) {
    return function (dispatch) {
        fetch(`https://backenda2.herokuapp.com/shoppingCarts/${id}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'delete',
        })
        .then((res)=>
                {return res.json()})
            .then((data)=>{
                console.log(data)
                dispatch({type: 'DELETE_CART', id: id})
        })
        .then(data=>dispatch(fetchCart())
    )
    }
}
export function updateCart(cart){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/shoppingCarts", {
            method: 'PUT',
            headers: {
                'Content-type' : 'application/json',
                'Accept' : 'application/json'

            },
            body: JSON.stringify(cart)
        })
        .then (function(res){
            return res.json()
        })
        .then(data=>dispatch(fetchCarts())
    )
    }
}