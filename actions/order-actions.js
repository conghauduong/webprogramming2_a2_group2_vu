export function fetchOrder() {
    return function (dispatch) {
        fetch('https://backenda2.herokuapp.com/categories')
            .then(function (res) {
                return res.json()
            })
            .then(function (data) {
                dispatch({
                    type: 'FETCH_ORDER_SUCCESS',
                    payload: data
                })
            })
    }
}

export function addOrder(orders){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/orders/", {
            method: 'POST',
            headers: {
                'Content-type' : 'application/json',
                'Accept' : 'application/json'

            },
            body: JSON.stringify(orders)
        })
        .then (function(res){
            return res.json()
        })
        .then(data=>dispatch(fetchOrder())
    )
    }
}
export function updateOrder(orders){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/orders/", {
            method: 'PUT',
            headers: {
                'Content-type' : 'application/json',
                'Accept' : 'application/json'

            },
            body: JSON.stringify(orders)
        })
        .then (function(res){
            return res.json()
        })
        .then(data=>dispatch(fetchOrder())
    )
    }
}


export function deleteOrder(id) {
    return function (dispatch) {
        fetch(`https://backenda2.herokuapp.com/orders/${id}`, {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            method: 'delete',
        })
        .then((res) => {
            return res.json()
        })
    }
}
export function getOrder(id) {
    return function (dispatch) {
        fetch(`https://backenda2.herokuapp.com/orders/${id}`, {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            method: 'get',
        })
        .then((res) => {
            return res.json()
        })
        .then(function(data){
            dispatch({type: 'EDIT_ORDER', payload: data })
        })
    }
}



 export function editedOrder(state = {name: '', age: 0}, action){
    if(action.type==='EDIT_ORDER'){
            console.log(action.payload)
            return action.payload
    }
    else{
        return state
    }
}
