export function fetchProducts(){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/products")
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({
                type: 'FETCH_PRODUCT_SUCCESS',
                products: data
            })
        })   
    }
}

//this version reloads the list of product, it is not efficient
// export function saveProduct(product){

//     return function(dispatch){
//         console.log(product)
//         fetch("http://bestlab.us:8080/products", {
//             headers: {
//                 'Accept': 'application/json, text/plain, */*',
//                 'Content-Type': 'application/json'
//               },
//             method: 'post', 
//             body: JSON.stringify(product)
//         })
//         .then((res)=>
//             {return res.json()})
//         .then((data)=>{
//             console.log(data)
//             dispatch(fetchProducts())
//         })   
// }
// }

export function saveProduct(product){
    return function(dispatch){
        fetch(`https://backenda2.herokuapp.com/products`, {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            method: 'post', 
            body: JSON.stringify(product)
        })
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({type:'ADD_PRODUCT_SUCCESS', product: data})
        })
    }
}

export function updateProduct(product){
    return function(dispatch){
        fetch('https://backenda2.herokuapp.com/products', {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            method: 'put', 
            body: JSON.stringify(product)
        })
        .then(function(res){
            return res.json()
        })
        .then(function(data){
            dispatch(fetchProducts())
        })
    }
}

//this version refresh the whole list which is not very good
// export function deleteProduct(productId){
    
//         return function(dispatch){
//             console.log(productId)
//             fetch(`http://bestlab.us:8080/products/${id}`, {
//                 method: 'delete', 
//             })
//             .then((res)=>
//                 {return res.json()})
//             .then((data)=>{
//                 console.log(data)
//                 dispatch(fetchProducts())
//             })   
//     }
//     }


export function deleteProduct(productId){
        return function(dispatch){
            console.log(productId)
            fetch(`https://backenda2.herokuapp.com/products${productId}`, {
                method: 'delete', 
            })
            .then((res)=>
                {return res.json()})
            .then((data)=>{
                console.log(data)
                dispatch({type: 'DELETE_PRODUCT', productId: productId})
            })
            .then(data=>dispatch(fetchProducts())
        )   
    }
}



export function editProduct(productId){
        return function(dispatch){
            fetch(`https://backenda2.herokuapp.com/products${productId}`, {
                method: 'GET', 
            })
            .then((res)=>
                {return res.json()})
            .then((data)=>{
                dispatch({type: 'EDIT_PRODUCT_SUCCESS', editedProduct: data})
            })   
    }
}
// export function addProductToCart(productId){
//     return function(dispatch){
//         fetch(`http://bestlab.us:8080/products/${productId}`, {
//             method: 'GET', 
//         })
//         .then((res)=>
//             {return res.json()})
//         .then((data)=>{
//             dispatch({type: 'ADD_PRODUCT_TO_CART', addedproduct: data})
//         })   
// }
// }



