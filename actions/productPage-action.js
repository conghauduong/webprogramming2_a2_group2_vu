export function fetchPage(){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/products")
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({
                type: 'FETCH_PAGE_SUCCESS',
                products: data
            })
        })   
    }
}
// var shoppingCart = [];
// function AddtoCart(name,description,price){
//     //Below we create JavaScript Object that will hold three properties you have mentioned:    Name,Description and Price
//     var singleProduct = {};
//     //Fill the product object with data
//     singleProduct.Name=name;
//     singleProduct.Description=description;
//     singleProduct.Price=price;
//     //Add newly created product to our shopping cart 
//     shoppingCart.push(singleProduct);
//     //call display function to show on screen
//     displayShoppingCart();

//  }  

export function updateCart(product){
    return function(dispatch){
        fetch(`https://backenda2.herokuapp.com/shoppingCarts`, {
                // headers: {
                //     'Accept': 'application/json, text/plain, */*',
                //     'Content-Type': 'application/json'
                // },
            method: 'post',
            body: JSON.stringify(product)
        })
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({type: 'ADD_CART_SUCCESS', data})
    })
    }
}



// export function updateCart(id){
//     return function(dispatch){
//         fetch('http://bestlab.us:8080/shoppingCarts/'+id)
//         .then(function(res){
//             return res.json()
//         })
//         .then(function(data){
//             dispatch({
//                 type: 'GET_CART',
//                 payload: data})
//         })
//         .then(function(dispatch1){
//             fetch(`http://bestlab.us:8080/shoppingCarts`, {
//                 // headers: {
//                 //     'Accept': 'application/json, text/plain, */*',
//                 //     'Content-Type': 'application/json'
//                 //   },
//                 method: 'post',
//                 body: JSON.stringify(data)})
//         })
//                 .then((res)=>
//             {return res.json()})
//         .then((data)=>{
//             dispatch({type: 'ADD_CART_SUCCESS', data})
//     })
//     }
// }
