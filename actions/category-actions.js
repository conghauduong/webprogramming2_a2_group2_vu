export function fetchCategorys(){
    return function(dispatch){
        fetch("https://backenda2.herokuapp.com/categories")
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({
                type: 'FETCH_CATEGORY_SUCCESS',
                categorys: data
            })
        })   
    }
}

//this version reloads the list of product, it is not efficient
// export function saveProduct(product){

//     return function(dispatch){
//         console.log(product)
//         fetch("http://bestlab.us:8080/products", {
//             headers: {
//                 'Accept': 'application/json, text/plain, */*',
//                 'Content-Type': 'application/json'
//               },
//             method: 'post', 
//             body: JSON.stringify(product)
//         })
//         .then((res)=>
//             {return res.json()})
//         .then((data)=>{
//             console.log(data)
//             dispatch(fetchProducts())
//         })   
// }
// }

export function saveCategory(category){
    return function(dispatch){
        fetch(`https://backenda2.herokuapp.com/categories`, {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            method: 'post', 
            body: JSON.stringify(category)
        })
        .then((res)=>
            {return res.json()})
        .then((data)=>{
            dispatch({type:'ADD_CATEGORY_SUCCESS', category: data})
        })
    }
}

export function updateCategory(category){
    return function(dispatch){
        fetch('https://backenda2.herokuapp.com/categories', {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            method: 'PUT', 
            body: JSON.stringify(category)
        })
        .then(function(res){
            return res.json()
        })
        .then(function(data){
            dispatch(fetchCategorys())
        })
    }
}
//this version refresh the whole list which is not very good
// export function deleteProduct(productId){
    
//         return function(dispatch){
//             console.log(productId)
//             fetch(`http://bestlab.us:8080/products/${id}`, {
//                 method: 'delete', 
//             })
//             .then((res)=>
//                 {return res.json()})
//             .then((data)=>{
//                 console.log(data)
//                 dispatch(fetchProducts())
//             })   
//     }
//     }


export function deleteCategory(categoryId){
        return function(dispatch){
            console.log(categoryId)
            fetch(`https://backenda2.herokuapp.com/categories/${categoryId}`, {
                method: 'delete', 
            })
            .then((res)=>
                {return res.json()})
            .then((data)=>{
                console.log(data)
                dispatch({type: 'DELETE_CATEGORY', categoryId: categoryId})
            })
            .then(data=>dispatch(fetchCategorys())
        )   
    }
}



export function editCategory(categoryId){
        return function(dispatch){
            fetch(`https://backenda2.herokuapp.com/categories/${categoryId}`, {
                method: 'GET', 
            })
            .then((res)=>
                {return res.json()})
            .then((data)=>{
                dispatch({type: 'EDIT_CATEGORY_SUCCESS', editedCategory: data})
            })   
    }
}


