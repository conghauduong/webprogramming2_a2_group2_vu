import React from 'react'
import App from './App.jsx'
import About from './About.jsx'
import {connect} from 'react-redux'
import ProductsView from './ProductsView.jsx'
import CartView from './CartView.jsx'
import OrderView from './OrderView.jsx'

class Root extends React.Component{
    render(){
        let currentPath = window.location.pathname
        return(
            <div>
                <div>
                    <a href='/home'>Home</a> |
                    <a href='/productsview'>View Products</a>|
                    <a href='/cartview'>View Cart</a>|
                    <a href='/orderview'>View Order</a>|
                    <a href='/about'>About us</a>
                    
                </div>   

                {currentPath.includes('/about')? 
                    <About />:
                    currentPath.includes('/productsview')? 
                    <ProductsView dispatch={this.props.dispatch} products={this.props.products} />:
                    currentPath.includes('/cartview')? 
                    <CartView dispatch={this.props.dispatch} products={this.props.products} 
                    carts={this.props.carts}/>:
                    currentPath.includes('/order')? 
                    <OrderView dispatch={this.props.dispatch} orders={this.props.orders} />:
                    <App 
                        dispatch={this.props.dispatch} 
                        products={this.props.products}
                        categorys={this.props.categorys}
                        editedProduct={this.props.editedProduct}
                        editedCategory={this.props.editedCategory}
                        carts={this.props.carts}
                          />
                }
            </div>    
        )
    }
}



function mapStateToProps(centralState) {
    return {
       products: centralState.productReducer,
       editedProduct: centralState.editedProductReducer,
       categorys: centralState.categoryReducer,
       editedCategory: centralState.editedCategoryReducer,
       carts :centralState.cartReducer,
       orders:centralState.orderReducer
       // addedproduct: centralState.productToCart
    }
 }
export default connect(mapStateToProps)(Root)
