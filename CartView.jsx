import React from 'react'
import {fetchCart, deleteCart, addToCart} from './actions/cart-actions.js'
import ShoppingCart from './components/ShoppingCart.jsx'
import './styles/styles.css'
import { fetchProducts } from './actions/product-actions';

export default class CartView extends React.Component {

    constructor(props){
      super(props)
    }
  
    componentDidMount() {
      // When container was mounted, we need to start fetching todos.
      this.props.dispatch(fetchCart())
    }

    render() {
        // let currentPath = window.location.pathname
          const { dispatch, products, carts } = this.props //,addedproduct
    
                return (
           <div className='container'>

                <div className='header'>
                <h1>Item Cart </h1>
              </div>
                <div className='row'>
                <div className='col5'>
                <ShoppingCart carts={carts} 
                      deleteCartClick = {(_id)=>dispatch(deleteCart(_id))}
                />
          
                </div>
                <div className='row'></div>
                </div>
              </div>
                )
            }
         }
              
    